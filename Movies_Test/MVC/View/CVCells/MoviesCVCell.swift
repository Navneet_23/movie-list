//
//  MoviesCVCell.swift
//  Movies_Test
//
//  Created by Navneet  on 29/05/21.
//

import UIKit

class MoviesCVCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgMovie: UIImageView!
    
    func setData(data : ListingModel){
        lblTitle.text = data.originalTitle
        let url = URL(string: imgBase + (data.posterPath ?? "" ))
        DispatchQueue.global(qos: .background).async {
            let img = LoadImage.getImageFromURL(imgUrl: (url ?? URL(string: ""))!)
            DispatchQueue.main.async {
                // only back on the main thread, may you access UI:
                self.imgMovie.image = img
                
            }
        }
    }
    
}
