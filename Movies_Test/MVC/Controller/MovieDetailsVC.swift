//
//  MovieDetailsVC.swift
//  Movies_Test
//
//  Created by Navneet  on 29/05/21.
//

import UIKit

class MovieDetailsVC: UIViewController {

    var data = ListingModel()
    
    @IBOutlet weak var lblDetails: UILabel!
    @IBOutlet weak var lblMovieName: UILabel!
    @IBOutlet weak var imgMovie: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var releaseDate: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setData(data: self.data)
    }
    
    func setData(data : ListingModel){
        self.lblTitle.text = "Overview"
        self.lblMovieName.text = data.originalTitle
        let url = URL(string: imgBase + (data.posterPath ?? "" ))
        self.imgMovie.image = LoadImage.getImageFromURL(imgUrl:(url ?? URL(string:""))!)
        self.lblDetails.text = data.overview
        self.lblRating.text = "Rating : \(data.voteAverage ?? 0.0) (\(data.voteCount ?? 0))"
        self.releaseDate.text = "Release date : \(data.releaseDate ?? "")"
    }

    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
}
