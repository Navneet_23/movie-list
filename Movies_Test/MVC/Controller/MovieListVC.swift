//
//  ViewController.swift
//  Movies_Test
//
//  Created by Navneet  on 29/05/21.
//

import UIKit

class MovieListVC: UIViewController {

    var moviesData = [ListingModel]()
    
    @IBOutlet weak var mainCV: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpcv()
        fetchMoviesData()
        
    }
    
    func setUpcv(){
        self.mainCV.dataSource = self
        self.mainCV.delegate = self
    }
    
    func fetchMoviesData() {
        do {
            let movies = try Data(contentsOf: Bundle.main.url(forResource: "movies", withExtension: "json")!)
            self.moviesData = try JSONDecoder().decode([ListingModel].self, from: movies)
            self.mainCV.reloadData()
        } catch {
            print(error)
        }
    }

}

extension MovieListVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.moviesData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MoviesCVCell", for: indexPath) as! MoviesCVCell
        let data = self.moviesData[indexPath.row]
        cell.setData(data: data)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dataM = self.moviesData[indexPath.row]
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MovieDetailsVC") as! MovieDetailsVC
        vc.data = dataM
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.view.layer.frame.width - 20) / 2
           return CGSize(width: width, height: width)
       }
    
}



