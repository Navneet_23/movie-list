
import Foundation
import UIKit

let securityKey = ""
struct WebService {
    
    static let boundary = "----WebKitFormBoundary7MA4YWxkTrZu0gW"
    
    // static var showJoinAlert:(()->())?
    
    static func service<Model: Codable>(_ api: API, param: Any? = nil, service: Services = .post ,showHud: Bool = true, response:@escaping (Model,Data,Any) -> Void)
    {
        if Reachability.isConnectedToNetwork()
        {
            var fullUrlString = api.rawValue
            
            if service == .get
            {
                if let parm = param{
                    if parm is String{
                        fullUrlString.append("?")
                        fullUrlString += (parm as! String)
                    }else if parm is Dictionary<String, Any>{
                        fullUrlString += self.getString(from: parm as! Dictionary<String, Any>)
                    }else{
                        assertionFailure("Parameter must be Dictionary or String.")
                    }
                }
            }
            print(fullUrlString)
            guard let encodedString = fullUrlString.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) else {return}
            var request = URLRequest(url: URL(string: encodedString)!, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 600)
            
            request.httpMethod = service.rawValue
            
            if let authKey = Store.authKey
            {
                print(authKey)
                request.addValue(authKey, forHTTPHeaderField: "auth_key")
                
            }
            request.addValue(securityKey, forHTTPHeaderField: "security_key")
            
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            if service == .delete || service == .put{
                request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                if let param = param{
                    if param is String{
                        let postData = NSMutableData(data: (param as! String).data(using: String.Encoding.utf8)!)
                        request.httpBody = postData as Data
                    }else if param is Dictionary<String, Any>{
                        var parm = self.getString(from: param as! Dictionary<String, Any>)
                        print(parm)
                        parm.removeFirst()
                        let postData = NSMutableData(data: parm.data(using: String.Encoding.utf8)!)
                        request.httpBody = postData as Data
                    }
                }
            }
            
            if service == .post
            {
                var param1: [String:Any] = param as? [String : Any] ?? [String:Any]()
                param1["security_key"] = securityKey
                param1["authorization_key"] = Store.authKey ?? ""
                //                if let parameter = param{
                //                    if parameter is String{
                //                        request.httpBody = (parameter as! String).data(using: .utf8)
                //                    }else if parameter is Dictionary<String, Any>{
                var body = Data()
                for (key, Value) in param1 {
                    print(key,Value)
                    if let imageInfo = Value as? ImageStructInfo{
                        body.append("--\(boundary)\r\n")
                        body.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(imageInfo.fileName)\"\r\n")
                        body.append("Content-Type: \(imageInfo.type)\r\n\r\n")
                        body.append(imageInfo.data)
                        body.append("\r\n")
                    }
                    else if let images = Value as? [ImageStructInfo]{
                        for (index,value) in images.enumerated(){
                            body.append("--\(boundary)\r\n")
                            body.append("Content-Disposition: form-data; name=\"\(key)[\(index)]\"; filename=\"\(value.fileName)\"\r\n")
                            body.append("Content-Type: \(value.type)\r\n\r\n")
                            body.append(value.data)
                            body.append("\r\n")
                        }
                    }else{
                        body.append("--\(boundary)\r\n")
                        body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                        body.append("\(Value)\r\n")
                    }
                }
                body.append("--\(boundary)--\r\n")
                request.httpBody = body
                //                    }else{
                //                        assertionFailure("Parameter must be Dictionary or String.")
                //                    }
                //                }
            }
            
            let sessionConfiguration = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfiguration)
            
            if showHud
            {
                DispatchQueue.main.async {
                    
                    
                    UIApplication.shared.isNetworkActivityIndicatorVisible = true
                    if let window = UIApplication.shared.keyWindow{
                        Spinner.start(from: (window.rootViewController?.view)!)
                        window.rootViewController?.view.isUserInteractionEnabled = false
                    }
                }
            }
            session.dataTask(with: request) { (data, jsonResponse, error) in
                if showHud{
                    DispatchQueue.main.async {
                        // SVProgressHUD.dismiss()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        if let window = UIApplication.shared.keyWindow
                        {
                            Spinner.stop()
                            window.rootViewController?.view.isUserInteractionEnabled = true
                        }
                    }
                }
                
                if error != nil{
                    DispatchQueue.main.async {
                        WebService.showAlert(error!.localizedDescription)
                    }
                }else{
                    if let jsonData = data{
                        do{
                            let jsonSer = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as! [String: Any]
                            print(jsonSer)
//                            let code = jsonSer["returnCode"] as? Int ?? 0
//                            if code == 400
//                            {
//                                DispatchQueue.main.async {
//
//                                    if let errorMessage = jsonSer["error_message"] as? String{
//
//                                        WebService.showAlert(errorMessage)
//                                    }else if let message = jsonSer["msg"] as? String{
//
//
//                                    }
//
//
//
//
//                                }
//                            }
//
//                            else if code == 205
//                            {
//                                DispatchQueue.main.async {
//                                    if let errorMessage = jsonSer["error_message"] as? String{
//                                                                          WebService.showAlert(errorMessage)
//                                                                      }else if let message = jsonSer["msg"] as? String{
//
//                                                                      }
//
//
//
//
//                                    //WebService.showJoinAlert?()
//                                    NotificationCenter.default.post(name: Notification.Name("showJoinAlert"), object: nil)
//
//
//                                }
//                            }
//                            else if code == 401
//                            {
//
//                                Store.autoLogin = false
//                                Store.remove = .userDetails
//                                Store.authKey = ""
//                                WebService.showLoginScreen()
//
//                            }
//
//
//                            else if code != 200
//                            {
//                                DispatchQueue.main.async {
//                                    if let errorMessage = jsonSer["error_message"] as? String{
//                                        WebService.showAlert(errorMessage)
//                                    }else if let message = jsonSer["msg"] as? String{
//
//                                    }
//                                }
//                            }
                                
                                
//                            else
//                            {
                                let decoder = JSONDecoder()
                                let model = try decoder.decode(Model.self, from: jsonData)
                                DispatchQueue.main.async {
                                    response(model,jsonData,jsonSer)
                                }
//                            }
                        }catch let err{
                            print(err)
                            DispatchQueue.main.async {
                                WebService.showAlert(err.localizedDescription)
                                // showSwiftyAlert(Title: "", body: err.localizedDescription, false)
                                
                            }
                        }
                    }
                }
            }.resume()
        }
        else
        {
            DispatchQueue.main.async {
                self.showAlert(noInternetConnection)
            }
        }
    }
    
    private static func showLoginScreen(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let viewController = storyboard.instantiateViewController(withIdentifier: "LoginNavVC")
        if let window = UIApplication.shared.keyWindow{
            window.rootViewController = viewController
        }
    }
    
    
    private static func showAlert(_ message: String)
    {
        if let window = UIApplication.shared.keyWindow
        {
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                window.rootViewController?.present(alert, animated: true, completion: nil)
            }

        }

    }
    
    private static func getString(from dict: Dictionary<String,Any>) -> String
    {
        var stringDict = String()
        stringDict.append("?")
        for (key, value) in dict{
            let param = key + "=" + "\(value)"
            stringDict.append(param)
            stringDict.append("&")
        }
        stringDict.removeLast()
        return stringDict
    }
}

extension Data {
    mutating func append(_ string: String) {
        if let data = string.data(using: .utf8){
            append(data)
        }
    }
}

extension UIImage
{
    func toData() -> Data{
        return self.jpegData(compressionQuality: 0.5)!
    }
}

struct ImageStructInfo
{
    var fileName: String
    var type: String
    var data: Data
}
