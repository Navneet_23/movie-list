
import UIKit

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

//extension NSObject {
//
//    class var className: String {
//        return String(describing: self)
//    }
//}


/// An extension to `UIImage` for creating images with shapes.
extension UIImage {
    
    /// Creates a circular outline image.
    class func outlinedEllipse(size: CGSize, color: UIColor, lineWidth: CGFloat = 1.0) -> UIImage? {
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        
        context.setStrokeColor(color.cgColor)
        context.setLineWidth(lineWidth)
        // Inset the rect to account for the fact that strokes are
        // centred on the bounds of the shape.
        let rect = CGRect(origin: .zero, size: size).insetBy(dx: lineWidth * 0.5, dy: lineWidth * 0.5)
        context.addEllipse(in: rect)
        context.strokePath()
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    func isEqualToImage(image: UIImage) -> Bool
    {
        let data1: Data = self.pngData()!
        let data2: Data = image.pngData()!
        return data1 == data2
       }
    
    class func getImageFromURL(imgUrl : String) -> UIImage{
        
        let url = URL(string:imgUrl)
        let data = try? Data(contentsOf: url!)

        if let imageData = data {
            let image = UIImage(data: imageData)!
            return(image)
        }
        return UIImage()
    }
}

class LoadImage {
    
    class func getImageFromURL(imgUrl : URL) -> UIImage{
        
        let data = try? Data(contentsOf: imgUrl)

        if let imageData = data {
            let image = UIImage(data: imageData)!
            return(image)
        }
        return UIImage()
    }
}

